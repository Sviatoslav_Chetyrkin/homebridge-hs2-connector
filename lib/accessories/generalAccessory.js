'use strict';
const Registry = require('./../Registry');
let Service, Characteristic;

class GeneralAccessory {

  constructor(options) {
    Service = Registry.get('Service');
    Characteristic = Registry.get('Characteristic');
    Object.keys(options).forEach(property => this[property] = options[property]);
    // this.informationService = null;

    this.informationService = new Service.AccessoryInformation()
      .setCharacteristic(Characteristic.Manufacturer, 'Homeseer2')
      .setCharacteristic(Characteristic.Model, this.type)
      .setCharacteristic(Characteristic.SerialNumber, this.getSerialNumber());

    this.accessoryService = null;
  }

  getSerialNumber(){
    return "HS2_" + this.type + "_id_" + this.id;
  }

  identify(callback) {
    callback(null); // success
  }

  getServices() {
    const services = [this.informationService];
    if (this.accessoryService) services.push(this.accessoryService);
    return services;
  }

}

module.exports = GeneralAccessory;