'use strict';
const Registry = require('./../Registry');
const GeneralAccessory = require('./generalAccessory');
let Service, Characteristic;

class GeneralEvent extends GeneralAccessory {

  constructor(options) {
    super(options);
    Service = Registry.get('Service');
    Characteristic = Registry.get('Characteristic');

    this.informationService = new Service.AccessoryInformation()
      .setCharacteristic(Characteristic.Manufacturer, 'Homeseer2')
      .setCharacteristic(Characteristic.Model, this.group)
      .setCharacteristic(Characteristic.SerialNumber, this.getSerialNumber());

    this.accessoryService = null;
  }

  getSerialNumber(){
    return "HS2_" + this.group + "_id_" + this.id;
  }

}

module.exports = GeneralEvent;