'use strict';

class Registry {
  constructor() {
  }

  static get(key) {
    return Registry.storage[key] || null;
  }

  static set(key, value) {
    Registry.storage[key] = value;
    return Registry;
  }
}
Registry.storage = {};
module.exports = Registry;

