'use strict';

const request = require('request');
const GeneralAccessory = require('./accessories/generalAccessory');
const GeneralEvent = require('./accessories/generalEvent');
const AccessoryDecorator = require('./accessoryDecorator');
// const decorator = new AccessoryDecorator();
const responseStub = require('../stub/responseStub');
const Registry = require('./Registry');
const Promise = require('bluebird');

class ConnectorPlatform {

  constructor(log, config, api) {
    log("Connector initialisation");
    this.log = log;
    this.config = config;
    this.api = api;
  }

  initializeEvents(callUrl) {
    let self = this;
    const events = [];
    const HttpCallQueue = Registry.get('HttpCallQueue');
    const decorator = new AccessoryDecorator();

    return HttpCallQueue.retryableGetRequest(callUrl)
      .then(parsedResponse => {
        parsedResponse
        // .filter(e => {
        //   if(!e.misc) return true;
        //   let status = parseInt(e.misc);
        //   let binaryRepresentation = status.toString(2);
        //   return !(binaryRepresentation.length > 1 && binaryRepresentation.charAt(1) === '1')
        // })
        // .filter(e => e.name.search(/(slideshow)/ig) >= 0)
          .filter(e => e.group && e.group.toLowerCase() === 'siri')
          .forEach(event => {
            event.address = self.config.address;
            // event.name = event.name + '_' + event.group + '_' + Math.floor(Math.random() * 99);
            const accessory = new GeneralEvent(event);
            events.push(decorator.decorateEvent(accessory));
          });
        return events;
      });
  }

  initializeGarageDoors(eventsUrl, devicesUrl) {
    let self = this;
    const decorator = new AccessoryDecorator();
    const garageDoors = [];
    const HttpCallQueue = Registry.get('HttpCallQueue');
    const garageDoorsConfig = self.config.garageDoors;
    return Promise.resolve(garageDoorsConfig)
      .then((config) =>{
        if (config && config.length > 0) {
          return Promise.props({
            events : HttpCallQueue.retryableGetRequest(eventsUrl),
            devices : HttpCallQueue.retryableGetRequest(devicesUrl)
          }).then(results => {
            const garageDoorObjects = config.map(door => {

              let garageDoorObject, garageDoorAccessory;

              if(door.garageSensor && door.garageOpenEvent) {
                const garageSensorName = door.garageSensor.toLowerCase();
                const garageEventName = door.garageOpenEvent.toLowerCase();

                const sensor = results.devices.find(d => d.name && d.name.toLowerCase() === garageSensorName);
                const event = results.events.find(e => e.name && e.name.toLowerCase() === garageEventName);

                if(sensor && event){
                  garageDoorObject = {};
                  garageDoorObject.sensorId = sensor.id;
                  garageDoorObject.eventId = event.id;
                  garageDoorObject.name = sensor.name;
                  garageDoorObject.address = self.config.address;
                  garageDoorObject.performingTime = door.performingTime || 10;
                  garageDoorAccessory = decorator.decorateGarageDoor(new GeneralAccessory(garageDoorObject));
                }
              }
              return garageDoorAccessory;
            });

            return garageDoorObjects;
          })
        }
        return [];
      });
  }

  initializeAccessories(callUrl) {
    let self = this;
    const accessories = [];
    const HttpCallQueue = Registry.get('HttpCallQueue');
    const decorator = new AccessoryDecorator();

    return HttpCallQueue.retryableGetRequest(callUrl)
      .then(parsedResponse => {
        parsedResponse
          .filter(a => a.floor && a.floor.toLowerCase() === 'siri')
          .forEach(device => {
            device.address = self.config.address;
            // Really nasty trick. But we should do if config actually has 2 separate similar accessories?
            // device.name = device.name + '_' + device.id + '_' + Math.floor(Math.random() * 99);
            const accessory = new GeneralAccessory(device);
            accessories.push(decorator.decorate(accessory));
          });
        return accessories;
      });
  }

  accessories(callback) {
    const self = this;
    const address =  this.config.address;
    console.log("Base Address: ", address);
    const devicesUrl = address + '/jsonapi.asp?action=getdevices';
    const eventsUrl = address + '/jsonapi.asp?action=getevents';


    return Promise.all([
      self.initializeAccessories(devicesUrl),
      self.initializeEvents(eventsUrl),
      self.initializeGarageDoors(eventsUrl, devicesUrl)
    ])
      .then(results => {
        // results[1] = results[1].slice(0, 10);
        const flatResult = [].concat.apply([], results).filter(element => !!element);
        return callback(flatResult);
      });

  }

  //Handler will be invoked when user try to config your plugin
  //Callback can be cached and invoke when nessary
  configurationRequestHandler(context, request, callback) {
    this.log("Configuring request handler");
    callback(null, "platform", true, {"platform":"HomeSeer2"});
  }

  // Sample function to show how developer can add accessory dynamically from outside event
  addAccessory(accessoryName) {
    this.log("Adding accessory");
  }

}

module.exports = ConnectorPlatform;