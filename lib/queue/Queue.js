'use strict';
const Task = require('./Task');

class Queue {
  constructor(delay) {
    this.queue = [];
    this.delay = delay || 500;
    // this.delay = 10;
    this.running = false;
    this._executeTasks();
  }

  add(fn, timeout) {
    console.log((new Date()).toUTCString(), 'task added');
    const task = new Task(fn, timeout);
    this.queue.push(task);
    return task.promise.then((result)=> {
      this.running = false;
      return result;
    })
      .catch((err)=>{
        this.running = false;
        throw new Error(err);
      });
  }

  _executeTasks() {
    if (!this.running && this.queue && this.queue.length) {
      const task = this.queue.pop();
      this.running = true;
      task.start();
    }
    setTimeout(()=> {
      this._executeTasks();
    }, this.delay)
  }
}

module.exports = new Queue();