'use strict';
const promise = require('bluebird');

class Task {
  constructor(fn, timeout) {
    this.state = 'inited';
    this.callable = fn;
    this.timeout = timeout || 10000;
    this.timer = null;
    this.ended = false;
    this.promise = new Promise((resolve, reject)=> {
      this.resolver = resolve;
      this.rejecter = reject;
    });
  }

  start() {
    console.log((new Date()).toUTCString(), 'task started');
    this.state = 'inProgress';
    this.callable().then((res)=> {
      this.state = 'done';
      console.log(res.data);
      this.end(res.data);
    }).catch((err)=> {
      this.state = 'fail';
      this.end(err);
    });
    this.timer = setTimeout(()=> {
      this.check()
    }, this.timeout)

  }

  check() {
    if (this.state === 'inProgress') {
      this.state = 'fail';
      this.end('Timeout Error!');
    }
  }

  end(data) {
    console.log((new Date()).toUTCString(), 'task ended with status', this.state);
    clearTimeout(this.timer);
    if (!this.ended) {
      switch (this.state) {
        case 'done':
          this.resolver(data);
          break;
        case 'fail':
          this.rejecter(data);
          break;
      }
    }
  }
}
module.exports = Task;