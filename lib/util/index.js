'use strict';
var crypto = require('crypto');

// http://stackoverflow.com/a/25951500/66673
function generateUUID(data) {
  var sha1sum = crypto.createHash('sha1');
  sha1sum.update(data);
  var s = sha1sum.digest('hex');
  var i = -1;
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    i += 1;
    switch (c) {
      case 'x':
        return s[i];
      case 'y':
        return ((parseInt('0x' + s[i], 16) & 0x3) | 0x8).toString(16);
    }
  });
}

function tryParseJSON (jsonString){
  // let jsonStringEncoded = encodeURI(jsonString);
  try {
    const o = JSON.parse(jsonString);
    // const o = JSON.parse(jsonStringEncoded);
    // Handle non-exception-throwing cases:
    // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
    // but... JSON.parse(null) returns null, and typeof null === "object",
    // so we must check for that, too. Thankfully, null is falsey, so this suffices:
    if (o && typeof o === "object") {
      return o;
    }
  }
  catch (e) {
    console.log(e)
  }
  return false;
}

module.exports = {
  generateUUID: generateUUID,
  tryParseJSON: tryParseJSON
};