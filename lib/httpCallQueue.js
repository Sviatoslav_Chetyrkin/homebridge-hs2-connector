'use strict';

const restling = require('restling');
const queue = require('./queue/Queue');
const util = require('./util');

class QueuedRequests {

  constructor(queue) {
  }

  get(url, callback) {
    queue.add(function () {
      return restling.get(url)
    }).then((result)=> {
        callback(null, {statusCode: 200}, result);
    }, (err) => {
      callback(err.message + " " + url);
    });
  }

  getPromise(url) {
    return queue.add(() => restling.get(url));
  }

  retryableGetRequest(url){
    let self = this;
    return self.getPromise(url)
      .then(result => {
        let parsedResponse = util.tryParseJSON(result);
        if (!parsedResponse) return self.retryableGetRequest(url);
        return parsedResponse;
      }, err => self.retryableGetRequest(url));
  }

}

module.exports = QueuedRequests;