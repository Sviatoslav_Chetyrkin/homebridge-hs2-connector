'use strict';
const Registry = require('./Registry');
const request = require('request');
const util = require('./util');
let Service, Characteristic;

function setPowerState(value, callback) {
    let self = this, tmpValue = false;
    const action = value ? 'deviceon' : 'deviceoff';
    const callUrl = encodeURI(this.address + '/jsonapi.asp?action=' + action + '&id=' + this.id);
    console.log("Calling for ", callUrl);
    console.log("[Set Power] ", action.substring(6).toUpperCase(), " for ", self.name);
    const HttpCallQueue = Registry.get('HttpCallQueue');
    HttpCallQueue.get(callUrl, (error, response, body) => {
        if (!error && response.statusCode == 200) {
            let reponseValue = JSON.parse(body).value;
            self.value = reponseValue;
            tmpValue = !!reponseValue && reponseValue > 0;
        }
        return callback(error, tmpValue)
    });
}

function getPowerState(callback) {
    let self = this;
    const callUrl = encodeURI(this.address + '/jsonapi.asp?action=getdevice&id=' + this.id);
    console.log("Calling for ", callUrl);
    console.log("[Get Power state] for ", self.name);
    const HttpCallQueue = Registry.get('HttpCallQueue');
    HttpCallQueue.get(callUrl, (error, response, body) => {
        let value = false;
        if (!error && response.statusCode == 200) {
            value = JSON.parse(body).value;
            self.value = value;
            console.log("[Get Power state] responded with the value ", self.value);
        }
        return callback(error, (!!value && value > 0))
    });
}

/******* For Testing: checking and return value ('on' or 'off') will be based on the device 'status' value *******/
function setPowerStateDim(value, callback) {
    let self = this, tmpValue = false;
    const action = value ? 'deviceon' : 'deviceoff';
    const callUrl = encodeURI(this.address + '/jsonapi.asp?action=' + action + '&id=' + this.id);
    console.log("Calling for ", callUrl);
    console.log("[Set Power] ", action.substring(6).toUpperCase(), " for ", self.name);
    const HttpCallQueue = Registry.get('HttpCallQueue');
    HttpCallQueue.get(callUrl, (error, response, body) => {
        let _res = false;
        if (!error && response.statusCode == 200) {
            let _status = JSON.parse(body).status;
            let _value = JSON.parse(body).value;
            self.status = _status;
            self.value = _value;
            console.log("[Get Power state] responded with status ", _status);
            _res = !!_status && _status == 2;
            console.log("[Get Power state] it is ", _res ? 'ON' : 'OFF');
        }
        return callback(error, tmpValue)
    });
}

function getPowerStateDim(callback) {
    let self = this;
    const callUrl = encodeURI(this.address + '/jsonapi.asp?action=getdevice&id=' + this.id);
    console.log("Calling for ", callUrl);
    console.log("[Get Power state] for ", self.name);
    const HttpCallQueue = Registry.get('HttpCallQueue');
    HttpCallQueue.get(callUrl, (error, response, body) => {
        let _res = false;
        if (!error && response.statusCode == 200) {
            let _status = JSON.parse(body).status;
            let _value = JSON.parse(body).value;
            self.status = _status;
            self.value = _value;
            console.log("[Get Power state] responded with status ", _status);
            _res = !!_status && _status == 2;
            console.log("[Get Power state] it is ", _res ? 'ON' : 'OFF');
        }
        return callback(error, _res);
    });
}
/******* /For Testing *******/

function getValue(callback) {
    const callUrl = encodeURI(this.address + '/jsonapi.asp?action=getdevice&id=' + this.id);
    console.log("Calling for ", callUrl);
    console.log("[Get Value] for ", this.name);
    const HttpCallQueue = Registry.get('HttpCallQueue');
    HttpCallQueue.get(callUrl, (error, response, body) => {
        let value = '';
        if (!error && response.statusCode == 200) {
            value = JSON.parse(body).value;
        }
        return callback(error, value)
    });
}

function getTemperature(callback) {
    let self = this;
    getValue.call(self, (err, value) => {
        console.log('[GetTemperature] sensor value ', value);
        return callback(err, Math.round(value / 10) / 10)
    });
}

function getDoorState(callback) {
    const callUrl = encodeURI(this.address + '/jsonapi.asp?action=getdevice&id=' + this.sensorId);
    console.log("Calling for ", callUrl);
    const HttpCallQueue = Registry.get('HttpCallQueue');
    HttpCallQueue.get(callUrl, (error, response, body) => {
        let value = '', device = {};
        if (!error && response.statusCode == 200) {
            device = JSON.parse(body);
        }
        const status = device.status === 3 ? 1 : 0;
        // const status = value > 0 ? 1 : 0;
        return callback(error, status)
    });
}

function checkDoorState() {
    let self = this;
    getDoorState.call(self, (err, value) => {
        self.accessoryService.getCharacteristic(Characteristic.CurrentDoorState).setValue(value);
        self.accessoryService.getCharacteristic(Characteristic.TargetDoorState).setValue(value);
    })
}

function toggleDoorState(value, callback) {
    let self = this;
    const callUrl = encodeURI(self.address + '/jsonapi.asp?action=runevent&id=' + self.eventId);
    console.log("Calling for ", callUrl);
    const HttpCallQueue = Registry.get('HttpCallQueue');
    HttpCallQueue.get(callUrl, (error, response, body) => {
        self.value = true;
        setTimeout(checkDoorState.bind(self), self.performingTime * 1000);
        return callback(error, value);
    });
}

function setValue(value, callback) {
    let self = this;
    const callUrl = encodeURI(this.address + '/jsonapi.asp?action=setdevicevalue&id=' + self.id + '&value=' + value);
    console.log("Calling for ", callUrl);
    const HttpCallQueue = Registry.get('HttpCallQueue');
    HttpCallQueue.get(callUrl, (error, response, body) => {
        if (!error && response.statusCode == 200) {
            self.value = JSON.parse(body).value;
        }
        return callback(error, self.value)
    });
}

function runEvent(value, callback) {
    let self = this;
    const callUrl = encodeURI(self.address + '/jsonapi.asp?action=runevent&id=' + self.id);
    console.log("Calling for ", callUrl);
    const HttpCallQueue = Registry.get('HttpCallQueue');
    HttpCallQueue.get(callUrl, (error, response, body) => {
        self.value = true;
        return callback(error, value);
    });
}

function getEventStatusStub(callback) {
    this.value = true;
    callback(null, true);
}


class Decorator {
    constructor() {
        Service = Registry.get('Service');
        Characteristic = Registry.get('Characteristic');
    }

    decorateEvent(accessory) {
        accessory.accessoryService = new Service.Switch();
        accessory.accessoryService
            .getCharacteristic(Characteristic.On)
            .on('set', runEvent.bind(accessory))
            .on('get', getEventStatusStub.bind(accessory));
        return accessory;
    }

    decorateGarageDoor(accessory) {
        // Characteristic.CurrentDoorState
        accessory.accessoryService = new Service.GarageDoorOpener();

        accessory.accessoryService
            .getCharacteristic(Characteristic.CurrentDoorState)
            .on('get', getDoorState.bind(accessory));

        accessory.accessoryService
            .getCharacteristic(Characteristic.TargetDoorState)
            .on('set', toggleDoorState.bind(accessory));

        return accessory;
    }

    decorate(accessory) {
        // TemperatureSensor
        const isSensor = accessory.type === 'Interface Variable';
        // const isSensor = accessory.type === 'Interface Variable.1';
        const bigValueLength = isSensor && accessory.value.toString().length > 2;
        // const temperatureSensor = isSensor && accessory.device_string === 'T6T';
        // const humiditySensor = isSensor && accessory.device_string === 'T6H';
        console.log('[Accessory]: ', accessory.name, ' isSensor ', isSensor, ' value ', accessory.value);
        const temperatureSensor = isSensor && bigValueLength;
        const humiditySensor = isSensor && !bigValueLength;
        //
        // accessory.device_string === 'T6H';
        // console.log('[Decorate Accessory]: ', accessory.name);
        switch (true) {
            case temperatureSensor: {
                accessory.accessoryService = new Service.TemperatureSensor();
                accessory.accessoryService
                    .getCharacteristic(Characteristic.CurrentTemperature)
                    .on('get', getTemperature.bind(accessory));
                break;
            }
            case humiditySensor: {
                accessory.accessoryService = new Service.HumiditySensor();
                accessory.accessoryService
                    .getCharacteristic(Characteristic.CurrentRelativeHumidity)
                    .on('get', getValue.bind(accessory));
                break;
            }
            case accessory.dimmable: {
                console.log('[Dimmable Accessory] ', accessory.name);
                accessory.accessoryService = new Service.Lightbulb();
                accessory.accessoryService
                    .getCharacteristic(Characteristic.Brightness)
                    .on('set', setValue.bind(accessory))
                    .on('get', getValue.bind(accessory));
                accessory.accessoryService
                    .getCharacteristic(Characteristic.On)
                    .on('set', setPowerStateDim.bind(accessory))
                    .on('get', getPowerStateDim.bind(accessory));
                    // .on('set', setPowerState.bind(accessory))
                    // .on('get', getPowerState.bind(accessory));
                break;
            }
            default: {
                accessory.accessoryService = new Service.Switch();
                accessory.accessoryService
                    .getCharacteristic(Characteristic.On)
                    .on('set', setPowerState.bind(accessory))
                    .on('get', getPowerState.bind(accessory));
                break;
            }
        }

        // if (accessory.dimmable){
        //   accessory.accessoryService = new Service.Lightbulb();
        //   accessory.accessoryService
        //     .getCharacteristic(Characteristic.Brightness)
        //     .on('set', setValue.bind(accessory))
        //     .on('get', getValue.bind(accessory));
        // } else {
        //   accessory.accessoryService = new Service.Switch();
        // }

        // accessory.accessoryService
        //   .getCharacteristic(Characteristic.On)
        //   .on('set', setPowerState.bind(accessory))
        //   .on('get', getPowerState.bind(accessory));
        return accessory;
    }

}

module.exports = Decorator;



