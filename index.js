'use strict';
// const http = require('http');
const ConnectorPlatform = require('./lib/connectorPlatform');
const Registry = require('./lib/Registry');
const HttpCallQueue = require('./lib/httpCallQueue');

module.exports = function (homebridge) {
  console.log("homebridge API version: " + homebridge.version);
  // Accessory must be created from PlatformAccessory Constructor
  Registry.set('Service', homebridge.hap.Service)
    .set('Accessory', homebridge.platformAccessory)
    .set('Characteristic', homebridge.hap.Characteristic)
    .set('UUIDGen', homebridge.hap.uuid)
    .set('HttpCallQueue', new HttpCallQueue());

  homebridge.registerPlatform("homebridge-hs2-connector", "HomeSeer2", ConnectorPlatform, true);
};

